import React, { useState } from "react";
import { Flex, Text, Img, Box } from "@chakra-ui/react";
import FileIcon from "assets/icons/File.svg";
import NavItem from "./NavItem";
import { useTranslation } from "react-i18next";
import { Icon } from "@chakra-ui/react";
import { HiChevronDown, HiChevronUp } from "react-icons/hi";
interface INavMultipleLevelItemProps {
  title: string;
  leftIcon?: any;
  description?: string;
  active?: boolean;
}

const NavMultipleLevelItem: React.FC<INavMultipleLevelItemProps> = ({
  leftIcon,
  title,
  active = false,
}) => {
  const [isOpen, setIsOpen] = useState<boolean>(true);
  const { t } = useTranslation();
  return (
    <Box style={{ display: "block", marginBottom: "16px", cursor: "pointer", width: '100%'}}>
      <Flex
        alignItems={"center"}
        justifyContent={"start"}
        width="100%"
        gap="12px"
        userSelect={'none'}
        onClick={() => {
          setIsOpen(!isOpen);
        }}
      >
        {leftIcon && (
          <Img src={leftIcon} alt="" width={"20px"} height={"20px"} />
        )}
        <Text
          display={"flex"}
          alignItems={"center"}
          color={active ? "#000" : "#777777"}
          fontSize={"14px"}
          fontWeight={active ? 500 : 400}
          lineHeight={1.15}
        >
          {title}
        </Text>
        <Icon
          as={isOpen ? HiChevronDown : HiChevronUp}
          color={"#000"}
          width={"1.2em"}
          height={"1.2em"}
        />
      </Flex>
      <Box mt={"12px"} display={isOpen ? "block" : "none"}>
        <NavItem
          title={t("drivers")}
          leftIcon={FileIcon}
          leftIconWidth="15px"
          leftIconHeight="15px"
          paddingLeft="15px"
        />
        <NavItem
          title={t("riders")}
          leftIcon={FileIcon}
          leftIconWidth="15px"
          leftIconHeight="15px"
          paddingLeft="15px"
          active={true}
        />
        <NavItem
          title={t("settings")}
          leftIcon={FileIcon}
          leftIconWidth="15px"
          leftIconHeight="15px"
          paddingLeft="15px"
        />
        <NavItem
          title={t("sign_up_login")}
          leftIcon={FileIcon}
          leftIconWidth="15px"
          leftIconHeight="15px"
          marginBottom="0px"
          paddingLeft="15px"
        />
      </Box>
    </Box>
  );
};
export default NavMultipleLevelItem;
