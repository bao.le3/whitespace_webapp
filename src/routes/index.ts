import { IRouter } from 'components/Router'
import homeRoutes from 'routes/home'
import workspaceRoutes from "routes/workspaces";
const routes: IRouter[] = [
  ...homeRoutes,
  ...workspaceRoutes
]

export default routes
