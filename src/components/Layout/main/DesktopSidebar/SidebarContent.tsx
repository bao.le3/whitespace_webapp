import React from "react";
import { Flex, Text, Img, Box } from "@chakra-ui/react";
import UserAvatar from "components/UserAvatar";
import { Link } from "react-router-dom";
import PlusIcon from "assets/icons/Plus.svg";
import EnvelopeOpenIcon from "assets/icons/EnvelopeOpen.svg";
import MixerVerticalIcon from "assets/icons/MixerVertical.svg";
import QuestionMarkCircledIcon from "assets/icons/QuestionMarkCircled.svg";
import AvatarIcon from "assets/icons/Avatar.svg";
import BrandIcon from "assets/icons/Brand.svg";
import ProductIcon from "assets/icons/Product.svg";
import WhitePlusIcon from "assets/icons/WhitePlus.svg";
import { useTranslation } from "react-i18next";
import NavItem from "./NavItem";
import NavMultipleLevelItem from "./NavMultipleLevelItem";
import { useAppSelector } from "app/hooks";
import { layoutSelector } from "../slice";
const SidebarContent: React.FC = () => {
  const {isCollapsedDektopSidebarOpen } = useAppSelector(layoutSelector);
  const { t } = useTranslation();
  return (
    <Flex flexDir={'column'} justifyContent={'space-between'} height={'100%'} width={'100%'}>
      <Flex
        flexDir="column"
        width="100%"
        height={'100%'}
        alignItems={"flex-start"}
        as="nav"
      >
        <Box
          as="button"
          width="100%"
          borderRadius={"30px"}
          backgroundColor={"black"}
          px={"12px"}
          py={"6px"}
          color={"white"}
          fontSize={"14px"}
          lineHeight={"14px"}
          fontWeight={"400"}
          height={"30px"}
          marginBottom={"24px"}
          backgroundImage={WhitePlusIcon}
          backgroundRepeat="no-repeat"
          backgroundPosition={"5% center"}
        >
          {t("create")}
        </Box>
        <NavItem
          title={t("inbox")}
          leftIcon={EnvelopeOpenIcon}
        />
        <NavItem
          title={t("settings")}
          leftIcon={MixerVerticalIcon}
        />
        <Link to="#" style={{ width: "100%", marginBottom: "16px" }}>
          <Flex
            mt={4}
            alignItems={"center"}
            justifyContent={"space-between"}
            width={"100%"}
          >
            <Text
              color="#777777"
              fontSize={"14px"}
              fontWeight={400}
              lineHeight={1.15}
            >
              {t("teamspaces")}
            </Text>
            <Img
              src={PlusIcon}
              alt="plus icon"
              width={"15px"}
              height={"15px"}
            />
          </Flex>
        </Link>
        <NavMultipleLevelItem
          title={t("products")}
          leftIcon={ProductIcon}
          active={true}
        />
        <NavItem
          title={t("brand")}
          leftIcon={BrandIcon}
        />
      </Flex>
      <Flex
        flexDir="column"
        w="100%"
        alignItems={"flex-start"}
        mb={isCollapsedDektopSidebarOpen ? '0px' : '20px'}
      >
        <NavItem
          title={t("invite_teammates")}
          leftIcon={AvatarIcon}
        />
        <NavItem
          title={t("help_support")}
          leftIcon={QuestionMarkCircledIcon}
        />
        <Flex mb={isCollapsedDektopSidebarOpen ? "20px" : "0px"} mt={"20px"} justifyContent={'center'} align="center" gap={"12px"}>
          <UserAvatar />
          <Text
            color="black"
            fontSize={"14px"}
            fontWeight={400}
            lineHeight={1.15}
          >
            Xiang Gao
          </Text>
        </Flex>
      </Flex>
    </Flex>
  );
};
export default SidebarContent;