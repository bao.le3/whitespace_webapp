import React, { useState } from "react";
import ReviewTemplatesList from "./ReviewTemplateList";
import ReviewTemplateDetail from "./ReviewTemplateDetail";
interface IReviewTemplateModalProps {
  isOpen: boolean;
  onClose: () => void;
  onOpen: () => void;
}
const ReviewTemplates: React.FC<IReviewTemplateModalProps> = ({
  isOpen,
  onClose,
  onOpen,
}) => {
  const [isOpenTemplateDetail, setIsOpenTemplateDetail] =
    useState<boolean>(false);
  const onChooseTemplate = () => {
    setIsOpenTemplateDetail(true);
  };
  return (
    <>
    {(!isOpenTemplateDetail) && <ReviewTemplatesList
      isOpen={isOpen}
      onClose={onClose}
      onOpen={onOpen}
      onChooseTemplate={onChooseTemplate}
    />}
    {(isOpenTemplateDetail) && <ReviewTemplateDetail
      isOpen={isOpen}
      onClose={onClose}
      onOpen={onOpen}
    />}
    </>
  );
};

export default ReviewTemplates;
