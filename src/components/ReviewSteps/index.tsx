import { Flex, Text } from "@chakra-ui/react";
import React from "react";
import { Icon } from "@chakra-ui/react";
import { HiOutlineChevronLeft, HiOutlineChevronRight } from "react-icons/hi";
// interface IReviewStepsProps {
//   handleOnNext: () => void;
//   handleOnPrev: () => void;
//   currentStep: number;
// }

const ReviewSteps: React.FC = () => {
  return (
    <Flex justifyContent={"space-between"} alignItems={"center"} gap={"6px"}>
      <Flex
        as="span"
        alignItems={"center"}
        justifyContent={"center"}
        width={"22px"}
        height={"22px"}
        cursor={"pointer"}
      >
        <Icon as={HiOutlineChevronLeft} width={4} height={4} color={"black"} />
      </Flex>
      <Text as="span" fontSize={"14px"}>
        1/6
      </Text>
      <Flex
        as="span"
        alignItems={"center"}
        justifyContent={"center"}
        width={"22px"}
        height={"22px"}
        cursor={"pointer"}
      >
        <Icon as={HiOutlineChevronRight} width={4} height={4} color={"black"} />
      </Flex>
    </Flex>
  );
};

export default ReviewSteps;
