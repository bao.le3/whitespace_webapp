import { Grid, GridItem } from '@chakra-ui/react';
import React from 'react';

const FrameThumbnail:React.FC = () => {
    return (
        <Grid templateColumns='repeat(5, 1fr)' gap={3} width={45 * 5}>
            <GridItem w='100%' h='4px' bg='#58B9F8' borderRadius={'10px'}/>
            <GridItem w='100%' h='4px' bg='#D9D9D9' borderRadius={'10px'}/>
            <GridItem w='100%' h='4px' bg='#D9D9D9' borderRadius={'10px'}/>
            <GridItem w='100%' h='4px' bg='#D9D9D9' borderRadius={'10px'}/>
            <GridItem w='100%' h='4px' bg='#D9D9D9' borderRadius={'10px'}/>
        </Grid>
    )
}

export default FrameThumbnail;