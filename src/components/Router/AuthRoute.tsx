import { Redirect, Route } from 'react-router-dom'

import { WHITESPACE_TOKEN } from 'constant/common'
import { IRouteProps } from 'components/Router/Router'
import React from 'react'

const AuthRoute: React.FC<IRouteProps> = ({ component: Component, ...rest }) => (
  <Route
    {...rest}
    render={(props) =>
      !window.localStorage.getItem(WHITESPACE_TOKEN) ? (
        <Component {...props} />
      ) : (
        <Redirect
          to={{
            pathname: `/`,
            state: { from: props.location },
          }}
        />
      )
    }
  />
)

export default AuthRoute
