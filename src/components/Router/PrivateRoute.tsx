import { Redirect, Route } from 'react-router-dom'

import { IRouteProps } from 'components/Router/Router'
import React from 'react'
import { clearStorageItems } from 'utils'
import { WHITESPACE_TOKEN, WHITESPACE} from 'constant/common'

const PrivateRoute: React.FC<IRouteProps> = ({ component: Component, ...rest }) => {
  const checkExpireTime = (): boolean => {
    if (typeof window !== 'undefined') {
      const storage = window.localStorage.getItem(WHITESPACE)
      if (!storage) return true

      const localStore = JSON.parse(storage)
      const getExpireTime = localStore.tokens?.access?.expires * 1000
      const isExpire = new Date() > new Date(getExpireTime)
      if (isExpire) {
        clearStorageItems([WHITESPACE_TOKEN])
      }
      return isExpire
    }
    return true
  }

  return (
    <Route
      {...rest}
      render={(props) =>
        window.localStorage.getItem(WHITESPACE_TOKEN) && !checkExpireTime() ? (
          <Component {...props} />
        ) : (
          <Redirect
            to={{
              pathname: `/login`,
              state: { from: props.location },
            }}
          />
        )
      }
    />
  )
}

export default PrivateRoute
