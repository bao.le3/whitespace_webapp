import React from "react";
import WorkspaceHeader from "./WorkSpacesHeader";
import { useLocation } from "react-router-dom";
import { CREATE_WORKSPACE_ROUTE, WORKSPACES_ROUTE } from "constant/router";
import CreateWorkspaceHeader from "./CreateWorkspaceHeader";
const DesktopHeader: React.FC = () => {
  const location = useLocation();
  if(location.pathname === WORKSPACES_ROUTE || location.pathname === '/') return <WorkspaceHeader/>
  if(location.pathname === CREATE_WORKSPACE_ROUTE) return <CreateWorkspaceHeader/>
  return <></>
};

export default DesktopHeader;
