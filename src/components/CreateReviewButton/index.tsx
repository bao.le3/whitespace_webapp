import {
  Flex,
  Text
} from "@chakra-ui/react";
import React, { useState } from "react";
import { Icon } from "@chakra-ui/react";
import { HiPlus } from "react-icons/hi";
import ReviewTemplates from "components/ReviewTemplates";
const CreateReviewButton: React.FC = () => {
  const [isOpen, setIsOpen] = useState<boolean>(false);
  const onClose = () => {
    setIsOpen(false);
  };
  const onOpen = () => {
    setIsOpen(true);
  };
  return (
    <>
      <Flex
        as="button"
        alignItems={"center"}
        gap={"6px"}
        justifyContent={"space-between"}
        onClick={onOpen}
      >
        <Flex
          as="span"
          alignItems={"center"}
          justifyContent={"center"}
          backgroundColor={"black"}
          borderRadius={"50%"}
          width={"22px"}
          height={"22px"}
        >
          <Icon as={HiPlus} width={4} height={4} color={"white"} />
        </Flex>
        <Text as="span" fontSize={"14px"}>
          Review
        </Text>
      </Flex>
      <ReviewTemplates isOpen={isOpen} onClose={onClose} onOpen={onOpen}/>
    </>
  );
};

export default CreateReviewButton;
