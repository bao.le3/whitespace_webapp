import { Flex, HStack, Text } from "@chakra-ui/react";
import CreatePost from "components/CreatePost";
import Nav from "components/Nav";
import UserAvatar from "components/UserAvatar";
import React from "react";
import { useTranslation } from "react-i18next";
import { useAppSelector } from "app/hooks";
import { layoutSelector } from "../slice";
import SidebarToggleButton from "components/SidebarToggleButton";
const WorkspacesHeader: React.FC = () => {
  const { t } = useTranslation();
  const { isDesktopSidebarOpen } = useAppSelector(layoutSelector);
  return (
    <Flex
      justify="space-between"
      alignItems={"center"}
      as="header"
      px={"20px"}
      height={"50px"}
      pos={"fixed"}
      width={isDesktopSidebarOpen ? "calc(100% - 260px)" : "100%"}
      backgroundColor={"white"}
      zIndex={50}
      top="0"
    >
      <Flex
        justifyContent={"start"}
        alignItems={"center"}
        gap="12px"
        height={"100%"}
      >
        <SidebarToggleButton />
        <Text
          as="a"
          fontSize="16px"
          fontWeight="500"
          textTransform={"capitalize"}
          lineHeight="16px"
        >
          {t("whitespace")}
        </Text>
      </Flex>
      <Nav />
      <HStack>
        <CreatePost />
        <UserAvatar />
      </HStack>
    </Flex>
  );
};

export default WorkspacesHeader;
