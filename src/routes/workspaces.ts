import { IRouter } from 'components/Router'
import { CREATE_WORKSPACE_ROUTE, WORKSPACES_ROUTE } from 'constant/router'

const routes: IRouter[] = [
  {
    path: WORKSPACES_ROUTE,
    loader: () => import('pages/WorkspaceDetail'),
    exact: true,
    type: 'public',
  },
  {
    path: CREATE_WORKSPACE_ROUTE,
    loader: () => import('pages/CreateWorkspace'),
    exact: true,
    type: 'public',
  },
]

export default routes
