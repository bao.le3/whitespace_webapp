import { Box, Flex } from '@chakra-ui/react';
import Workspaces from 'components/Workspaces';
import Search from 'components/Search';
import ViewToggleButton from 'components/ViewToggleButton';
import React from 'react';

const Home: React.FC = () => {
    return (
        <Box width={'90%'} mx={'auto'} py={'24px'}>
            <Flex justifyContent={'space-between'} gap={'12px'}>
                <Search/>
                <ViewToggleButton/>
            </Flex>
            <Box mt={'25px'}>
                <Workspaces/>
            </Box>
        </Box>
    )
}

export default Home;