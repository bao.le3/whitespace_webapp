export const clearStorageItems = (keys: string[]): void => {
    for (const key of keys) {
        window.localStorage.removeItem(key)
    }
}