import { Box, Flex, Text, Img, Input } from "@chakra-ui/react";
import React from "react";
import { useForm } from "react-hook-form";
import * as yup from "yup";
import { yupResolver } from "@hookform/resolvers/yup";
import i18n from "locales/i18n";
import ContentEditableField from "components/FormFields/ContentEditableField";
import { useTranslation } from "react-i18next";
import Figma from "assets/icons/Figma.png";
import Loom from "assets/icons/Loom.png";
interface IWorkspace {
  title: string;
  desc: string;
}
const schema = yup.object().shape({
  title: yup.string().required(i18n.t("required_title")),
});
const CreateWorkspace: React.FC = () => {
  const { t } = useTranslation();
  const { control, handleSubmit } = useForm<IWorkspace>({
    defaultValues: {
      title: "",
      desc: "",
    },
    resolver: yupResolver(schema),
  });
  const handleFormSubmit = async (formValues: IWorkspace) => {
    console.log(formValues);
  };
  return (
    <Box width={"55%"} mx={"auto"} mt={"40px"}>
      <form onSubmit={handleSubmit(handleFormSubmit)}>
        <Box mb="12px">
          <ContentEditableField
            name="title"
            control={control}
            placeholder={t("untitled")}
            className="whitespace-content-editable-title"
          />
        </Box>
        <Box mb="20px">
          <ContentEditableField
            name="desc"
            control={control}
            placeholder={t("write_a_description_of_this_post")}
            className="whitespace-content-editable-desc"
          />
        </Box>
        <Flex
          justifyContent={"start"}
          alignItems={"center"}
          mb="14px"
          backgroundColor={"white"}
          width={"100%"}
          px={"20px"}
          py={"16px"}
          border="1px solid #E1E1E1"
          borderRadius={"8px"}
          gap={'12px'}
        >
          <Input
            flexBasis={"80%"}
            outline={"none"}
            border={"none"}
            padding={"0px"}
            fontSize={"28px"}
            color="black"
            fontWeight={500}
            placeholder={t("add_a_link")}
            css={`
              &:focus-visible {
                outline: none;
                box-shadow: none;
              }
              &::placeholder {
                color: #000000;
              }
              &::-webkit-input-placeholder {
                color: #000000;
              }
              &:-ms-input-placeholder {
                color: #000000;
              }
            `}
          />
          <Flex alignItems={"center"} justifyContent={"start"} gap={"12px"}>
            <Text color={"#777777"} fontSize={"14px"}>
              {t("supports")}:
            </Text>
            <Img src={Figma} alt="" width={"15px"} height={"20px"} />
            <Img src={Loom} alt="" width={"15px"} height={"20px"} />
          </Flex>
        </Flex>
        <Flex alignItems={"center"} justifyContent={"start"} gap='12px'>
          <Text color={"#777777"} fontSize={"14px"}>
            {t("supports")}:
          </Text>
          <Img src={Figma} alt="" width={"15px"} height={"20px"} />
          <Img src={Loom} alt="" width={"15px"} height={"20px"} />
        </Flex>
      </form>
    </Box>
  );
};

export default CreateWorkspace;
