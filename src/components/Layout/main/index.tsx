import React, { ReactNode, useCallback } from "react";
import { Box, Drawer, DrawerContent } from "@chakra-ui/react";
import MobileSidebar from "./MobileSidebar";
import DesktopSidebar from "./DesktopSidebar";
import { useAppSelector, useAppDispatch } from "app/hooks";
import { layoutSelector, layoutActions } from "components/Layout/main/slice";
import DesktopHeader from "./DesktopHeader";
import SidebarContent from "./DesktopSidebar/SidebarContent";
import classNames from "classnames";
import "./style.scss";
interface ILayoutProps {
  children: ReactNode;
}
const Layout: React.FC<ILayoutProps> = ({ children }) => {
  const {
    isMobileSidebarOpen,
    isDesktopSidebarOpen,
    isCollapsedDektopSidebarOpen,
  } = useAppSelector(layoutSelector);
  const dispatch = useAppDispatch();
  const onClose = useCallback(() => {
    dispatch(layoutActions.toggleMobileSidebar({ isOpen: false }));
  }, []);
  return (
    <Box minH="100vh" bg={"#F5F5F5"}>
      {isDesktopSidebarOpen && <DesktopSidebar />}
      {(!isDesktopSidebarOpen) && (
        <Box
          width={"260px"}
          pos="fixed"
          pt={"70px"}
          left={0}
          top={"0"}
          height={"90vh"}
          className={classNames("sidebar-content", {
            "sidebar-content--show": isCollapsedDektopSidebarOpen,
            "sidebar-content--hidden": !isCollapsedDektopSidebarOpen,
          })}
          onMouseOver={() => {
            dispatch(
              layoutActions.toggleCollapsedDesktopSidebar({ isOpen: true })
            );
          }}
          onMouseLeave={() => {
            dispatch(
              layoutActions.toggleCollapsedDesktopSidebar({ isOpen: false })
            );
          }}
        >
          <Box
            backgroundColor={"white"}
            p={"20px"}
            overflowY={"scroll"}
            height={'100%'}
            boxShadow="0 4px 12px 0 rgba(0, 0, 0, 0.05)"
          >
            <SidebarContent />
          </Box>
        </Box>
      )}
      <Drawer
        autoFocus={false}
        isOpen={isMobileSidebarOpen}
        placement="left"
        onClose={onClose}
        returnFocusOnClose={false}
        onOverlayClick={onClose}
        size="full"
      >
        <DrawerContent>
          <MobileSidebar onClose={onClose} />
        </DrawerContent>
      </Drawer>
      <Box
        ml={{ base: 0, md: isDesktopSidebarOpen ? "260px" : 0 }}
        pt={"70px"}
        transition={"all 0.5s"}
      >
        <DesktopHeader />
        {children}
      </Box>
    </Box>
  );
};

export default Layout;
