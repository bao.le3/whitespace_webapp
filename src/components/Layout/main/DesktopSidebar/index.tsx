import React from "react";
import { Flex, Text, IconButton, Img, Box } from "@chakra-ui/react";
import { FiMenu } from "react-icons/fi";
import Logo from "assets/img/Logo.png";
import { useAppDispatch, useAppSelector } from "app/hooks";
import { layoutActions, layoutSelector } from "../slice";
import SidebarContent from "components/Layout/main/DesktopSidebar/SidebarContent";
const Sidebar: React.FC = () => {
  const { isDesktopSidebarOpen, isCollapsedDektopSidebarOpen } = useAppSelector(layoutSelector);
  const dispatch = useAppDispatch();
  return (
    <Flex
      pos="fixed"
      px={"30px"}
      h="100vh"
      boxShadow="0 4px 12px 0 rgba(0, 0, 0, 0.05)"
      width={"260px"}
      flexDir="column"
      justifyContent="start"
      backgroundColor={"white"}
      transition="all 0.3s ease-in-out"
      overflowY={isCollapsedDektopSidebarOpen ? 'scroll' : 'auto'}
    >
      <Flex
        justifyContent={"space-between"}
        alignItems={"center"}
        width={"100%"}
        marginBottom={"20px"}
        mt={5}
      >
        <Flex justifyContent={"start"} alignItems={"center"} gap="8px">
          <Img src={Logo} alt="logo" width={"40px"} />
          <Text as={"h1"}>
            Uber
          </Text>
        </Flex>
        <IconButton
          aria-label=""
          background="none"
          _hover={{ background: "none" }}
          icon={<FiMenu />}
          onClick={() => {
            if (!isDesktopSidebarOpen)
              dispatch(layoutActions.toggleDesktopSidebar({ isOpen: true }));
            else dispatch(layoutActions.toggleDesktopSidebar({ isOpen: false }));
          }}
        />
      </Flex>
      <Box flexBasis={'90%'}><SidebarContent/></Box>
    </Flex>
  );
};

export default Sidebar;
