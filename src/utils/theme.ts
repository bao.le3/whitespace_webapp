import { extendTheme } from "@chakra-ui/react";
const breakpoints = {
    sm: '540px',
    md: '768px',
    lg: '960px',
    xl: '1200px',
    '2xl': '1400px',
  }
export const theme = extendTheme({
    fonts: {
        heading: `sans-serif`,
        body: `sans-serif`,
    },
    components: {
        Modal: {
            sizes: {
                '700px': '700px'
            }
        }
    },
    breakpoints    
});