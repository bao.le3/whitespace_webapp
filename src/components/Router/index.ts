import Router, { IRouteProps, IRouter } from 'components/Router/Router'

export default Router
export type { IRouteProps, IRouter }
