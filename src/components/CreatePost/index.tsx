import {Flex, Text } from "@chakra-ui/react";
import { Icon } from '@chakra-ui/react';
import { HiPlus } from 'react-icons/hi';
import React from "react";
import { useTranslation } from "react-i18next";
const CreatePost: React.FC = () => {
  const { t } = useTranslation();
  return (
    <Flex
      as="button"
      color={"white"}
      borderRadius={"36px"}
      alignItems={"center"}
      justifyContent={'center'}
      gap={"5px"}
      backgroundColor={"black"}
      width={"75px"}
      height={"34px"}
      _hover={{color: 'black', backgroundColor: 'gray.200'}}
      fontSize={'14px'}
    >
      <Icon as={HiPlus}/>
      <Text textTransform={"capitalize"} fontWeight={500}>{t("post")}</Text>
    </Flex>
  );
};

export default CreatePost;
