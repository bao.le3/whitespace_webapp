import React from "react";
import { Flex, Icon } from "@chakra-ui/react";
import { HiChevronDoubleRight } from "react-icons/hi";
import { useAppDispatch, useAppSelector } from "app/hooks";
import { layoutActions, layoutSelector } from "components/Layout/main/slice";
import { FiMenu } from "react-icons/fi";
const SidebarToggleButton: React.FC = () => {
  const { isDesktopSidebarOpen, isCollapsedDektopSidebarOpen} = useAppSelector(layoutSelector);
  const dispatch = useAppDispatch();
  if (!isDesktopSidebarOpen) {
    return (
      <Flex
        height={"100%"}
        width={'30px'}
        alignItems={"center"}
        cursor={"pointer"}
      >
        <Icon
          as={isCollapsedDektopSidebarOpen ? HiChevronDoubleRight : FiMenu}
          width={"30px"}
          height={"30px"}
          padding={'5px'}
          onMouseOver={() => {
            dispatch(
              layoutActions.toggleCollapsedDesktopSidebar({ isOpen: true })
            );
          }}
          onClick={() => {
            dispatch(
              layoutActions.toggleDesktopSidebar({
                isOpen: !isDesktopSidebarOpen,
              })
            );
          }}
        _hover={{backgroundColor: '#f5f5f5'}}
        />
      </Flex>
    );
  }
  return <></>;
};

export default SidebarToggleButton;
