import { Grid, GridItem } from '@chakra-ui/react';
import React from 'react';
import WorkspaceItem from './WorkspaceItem';

const Reviews:React.FC = () => {
    return (
        <Grid templateColumns={{base: 'repeat(3, 1fr)', '2xl': 'repeat(4, 1fr)'}} gap={'35px'}>
            <GridItem w='100%'>
                <WorkspaceItem/>
            </GridItem>
            <GridItem w='100%'>
                <WorkspaceItem/>
            </GridItem>
            <GridItem w='100%'>
                <WorkspaceItem/>
            </GridItem>
            <GridItem w='100%'>
                <WorkspaceItem/>
            </GridItem>
            <GridItem w='100%'>
                <WorkspaceItem/>
            </GridItem>
            <GridItem w='100%'>
                <WorkspaceItem/>
            </GridItem>
            <GridItem w='100%'>
                <WorkspaceItem/>
            </GridItem>
            <GridItem w='100%'>
                <WorkspaceItem/>
            </GridItem>
            <GridItem w='100%'>
                <WorkspaceItem/>
            </GridItem>
            <GridItem w='100%'>
                <WorkspaceItem/>
            </GridItem>
            <GridItem w='100%'>
                <WorkspaceItem/>
            </GridItem>
            <GridItem w='100%'>
                <WorkspaceItem/>
            </GridItem>
        </Grid>
    );
}

export default Reviews;