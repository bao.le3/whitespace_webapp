import { Box, Flex, Image, Text } from "@chakra-ui/react";
import React from "react";
import Image1399 from "assets/img/image1399.png";
import { useHistory } from "react-router-dom";
import { WORKSPACES_ROUTE } from "constant/router";
function getRandomColor() {
  var letters = "0123456789ABCDEF";
  var color = "#";
  for (var i = 0; i < 6; i++) {
    color += letters[Math.floor(Math.random() * 16)];
  }
  return color;
}
const WorkspaceItem: React.FC = () => {
  const history = useHistory();
  const color = getRandomColor();
  const handleOnClick = () => {
    history.push(WORKSPACES_ROUTE);
  };
  return (
    <Box>
      <Flex alignItems={"center"} justifyContent="space-between" mb={"10px"}>
        <Flex
          alignItems={"center"}
          justifyContent={"space-between"}
          gap={"6px"}
        >
          <Flex
            backgroundColor={color}
            width={"20px"}
            height={"20px"}
            borderRadius={"50%"}
            justifyContent={"center"}
            alignItems={"center"}
          >
            <Text fontSize={"10px"} color={`#fff`} fontWeight={500}>
              XL
            </Text>
          </Flex>
          <Text as="span" fontSize={"12px"}>
            Xiang Chao
          </Text>
        </Flex>
        <Text as="span" color="#777777" fontSize={"12px"}>
          12 hrs ago
        </Text>
      </Flex>
      <Box
        px={"75px"}
        py={"33px"}
        backgroundColor={"white"}
        borderRadius={"12px"}
        cursor={"pointer"}
        onClick={handleOnClick}
      >
        <Image src={Image1399} alt="" mx={'auto'}/>
      </Box>
      <Box mt={"8px"}>
        <Text
          as="h3"
          fontWeight={600}
          fontSize={"12px"}
          color={"black"}
          cursor={"pointer"}
          onClick={handleOnClick}
        >
          Easy Airport Access V1
        </Text>
        <Text
          as="p"
          fontWeight={400}
          fontSize={"12px"}
          color={"#777777"}
          mt={"4px"}
        >
          New way to easily request a ride to the nearest major airport.
        </Text>
        <Flex
          justifyContent={"flex-start"}
          alignItems={"center"}
          gap={"6px"}
          mt="12px"
        >
          <Text
            as="span"
            backgroundColor={"white"}
            color={"#777777"}
            borderRadius={"30px"}
            px={"6px"}
            fontSize={"10px"}
          >
            iOS
          </Text>
          <Text
            as="span"
            backgroundColor={"white"}
            color={"#777777"}
            borderRadius={"30px"}
            px={"6px"}
            fontSize={"10px"}
          >
            Airport
          </Text>
          <Text
            as="span"
            backgroundColor={"white"}
            color={"#777777"}
            borderRadius={"30px"}
            px={"6px"}
            fontSize={"10px"}
          >
            Custom Component
          </Text>
        </Flex>
      </Box>
    </Box>
  );
};

export default WorkspaceItem;
