import React from "react";
import { Flex, Text, Img } from "@chakra-ui/react";
import { Link } from "react-router-dom";
interface INavItemProps {
  title: string,
  leftIcon?: any,
  leftIconWidth?: string,
  leftIconHeight?: string,
  leftIconAlt?: string,
  description?: string,
  active?: boolean,
  marginBottom?: string,
  paddingLeft?: string 
}

const NavItem: React.FC<INavItemProps> = ({
  leftIcon,
  title,
  leftIconWidth = "20px", 
  leftIconHeight = "20px",
  marginBottom = "16px",
  paddingLeft = "0px",
  active = false
}) => {
  return (
    <Link to="" style={{ display: "block", marginBottom: marginBottom }}>
      <Flex
        alignItems={"center"}
        justifyContent={"start"}
        width="100%"
        gap="12px"
        backgroundColor={active ? '#F5F5F5' : 'transparent'}
        padding={'10px'}
        borderRadius={'30px'}
        pl={paddingLeft}
      >
        {leftIcon && (
          <Img src={leftIcon} alt="" width={leftIconWidth} height={leftIconHeight} />
        )}
        <Text
          color={active ? "#000" : "#777777"}
          fontSize={"14px"}
          fontWeight={active ? 500 : 400}
          lineHeight={1.15}
        >
          {title}
        </Text>
      </Flex>
    </Link>
  );
};
export default NavItem;
