import { Input, InputGroup, InputLeftElement } from "@chakra-ui/react";
import React from "react";
import { Icon } from '@chakra-ui/react';
import { HiSearch } from 'react-icons/hi';
const Search: React.FC = () => {
  return (
    <InputGroup>
      <InputLeftElement
        pointerEvents="none"
        children={<Icon as={HiSearch} color={'#777777'} fontSize={'18px'}/>}
      />
      <Input
        type="text"
        placeholder="Search designs, teammates, and tags"
        borderRadius={"30px"}
        backgroundColor={"white"}
      />
    </InputGroup>
  );
};

export default Search;
