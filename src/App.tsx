import React from 'react';
import './App.css';
import { Provider } from 'react-redux';
import { store } from './app/store';
import "locales/i18n";
import { ConnectedRouter } from 'connected-react-router';
import { history } from 'utils';
import { ChakraProvider } from '@chakra-ui/react';
import routes from 'routes';
import Router from 'components/Router';
import {theme} from "utils";
function App() {
  return (
    <React.StrictMode>
      <Provider store={store}>
      <ConnectedRouter history={history}>
        <ChakraProvider theme={theme}>
            <Router routes={routes} />
        </ChakraProvider>
      </ConnectedRouter>
      </Provider>
    </React.StrictMode>
  );
}

export default App;
