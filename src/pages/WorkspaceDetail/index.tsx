import { Box, Flex, Image, Text } from "@chakra-ui/react";
import BottomToolbar from "components/BottomToolbar";
import Breadcrumb from "components/Breadcrumb";
import FrameThumbnail from "components/FrameThumbnail";
import React from "react";
import Image1399 from "assets/img/image1399.png";
const WorkspaceDetail: React.FC = () => {
  return (
    <>
      <Flex p={"20px"} direction={"column"}>
        <Flex alignItems={"center"} justifyContent={"space-between"}>
          <Breadcrumb />
          <FrameThumbnail />
          <Flex alignItems={"center"} justifyContent={"start"} gap={"6px"}>
            <Box
              as="button"
              borderWidth={"2px"}
              borderStyle={"solid"}
              borderColor={"#E1E1E1"}
              borderRadius={"4px"}
              backgroundColor={"white"}
              px={"12px"}
              py={"6px"}
              color={"#777777"}
              fontSize={"14px"}
              lineHeight={"16px"}
              fontWeight={"400"}
            >
              Save & exit
            </Box>
            <Box
              as="button"
              borderRadius={"4px"}
              backgroundColor={"#58B9F8"}
              px={"12px"}
              py={"6px"}
              color={"white"}
              fontSize={"14px"}
              lineHeight={"16px"}
              fontWeight={"400"}
            >
              Preview
            </Box>
            <Box
              as="button"
              borderRadius={"4px"}
              backgroundColor={"black"}
              px={"12px"}
              py={"6px"}
              color={"white"}
              fontSize={"14px"}
              lineHeight={"16px"}
              fontWeight={"400"}
            >
              Share
            </Box>
          </Flex>
        </Flex>
        <Flex justifyContent={"center"}>
          <Box mt={"38px"}>
            <Text
              as="h5"
              fontSize={"12px"}
              lineHeight={"14px"}
              fontWeight={400}
              color="#777777"
              mb={'4px'}
            >
              Easy Airport Access 1
            </Text>
            <Image src={Image1399} alt="" height={'75vh'}/>
          </Box>
        </Flex>
      </Flex>
      <BottomToolbar />
    </>
  );
};

export default WorkspaceDetail;
