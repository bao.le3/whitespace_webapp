import { IRouter } from 'components/Router'

const routes: IRouter[] = [
  {
    path: '/',
    loader: () => import('pages/WorkspacesList'),
    exact: true,
    type: 'public',
  },
  {
    path: '/home',
    loader: () => import('pages/WorkspacesList'),
    exact: true,
    type: 'public',
  },
]

export default routes
