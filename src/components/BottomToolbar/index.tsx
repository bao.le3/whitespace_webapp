import { Flex } from "@chakra-ui/react";
import CreateReviewButton from "components/CreateReviewButton";
import ReviewSteps from "components/ReviewSteps";
import React from "react";
const BottomToolbar: React.FC = () => {
  return (
    <Flex
      as="footer"
      pos={"fixed"}
      bottom={"0%"}
      left={0}
      backgroundColor={"white"}
      width={"100%"}
      px={"10px"}
      py={"10px"}
      justifyContent={'space-between'}
      alignItems={'center'}
    >
      <CreateReviewButton/>
      <ReviewSteps/>
    </Flex>
  );
};
export default BottomToolbar;
