import { Flex, Text } from "@chakra-ui/react";
import React from "react";

const UserInfo: React.FC = () => {
  return (
    <Flex alignItems={"center"} justifyContent={"space-between"} gap={"6px"}>
      <Flex
        backgroundColor={"#F5CEDB"}
        width={"20px"}
        height={"20px"}
        borderRadius={"50%"}
        justifyContent={"center"}
        alignItems={"center"}
      >
        <Text fontSize={"12px"} color={"#C60045"} fontWeight={500}>
          XL
        </Text>
      </Flex>
      <Text as="span" fontSize={"12px"}>
        Xiang Chao
      </Text>
    </Flex>
  );
};

export default UserInfo;
