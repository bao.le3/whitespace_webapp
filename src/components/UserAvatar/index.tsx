import { Flex, Text } from "@chakra-ui/react";
import React from "react";

const UserAvatar: React.FC = () => {
  return (
    <Flex
      backgroundColor={"#DFEAFF"}
      width={"35px"}
      height={"35px"}
      borderRadius={"50%"}
      justifyContent={'center'}
      alignItems={'center'}
    >
      <Text fontSize={"12px"} color={"#3478F5"} fontWeight={500}>XL</Text>
    </Flex>
  );
};

export default UserAvatar;
