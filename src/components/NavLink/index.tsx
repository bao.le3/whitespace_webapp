import React from "react";
import { Link } from "react-router-dom";
import { useLocation } from "react-router-dom";
import { Text } from "@chakra-ui/react";
interface INavLinkProps {
  to: string;
  name: string;
}
const NavLink: React.FC<INavLinkProps> = ({ to, name }) => {
  const location = useLocation();
  const isActive = location.pathname === to;
  if (isActive) {
    return (
      <Link to={to}>
        <Text
          as="span"
          backgroundColor={"#E9E9E9"}
          px={"20px"}
          py={"6px"}
          borderRadius={"36px"}
          fontWeight={500}
        >
          {name}
        </Text>
      </Link>
    );
  }
  return (
    <Link to={to}>
      <Text as="span">{name}</Text>
    </Link>
  );
};

export default NavLink;
