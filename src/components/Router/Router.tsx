import { Route, Switch } from 'react-router-dom'
import loadable, { LoadableComponent } from '@loadable/component'

import { DefaultComponent } from '@loadable/component'
import FallbackScreen from 'components/Router/FallbackScreen'
import Layout from 'components/Layout/main'
import React from 'react'

import PrivateRoute from 'components/Router/PrivateRoute'
import AuthRoute from 'components/Router/AuthRoute'

export interface IRouter {
  loader: () => Promise<DefaultComponent<any>>
  path: string
  exact: boolean
  type: 'public' | 'private' | 'auth'
}

interface IProps {
  routes: IRouter[]
}

export interface IRouteProps {
  path: string
  exact: boolean
  component: LoadableComponent<any>
}

const Router: React.FC<IProps> = ({ routes }) => {
  const routeSelector = (router: IRouter, key: number) => {
    const { path, exact, loader, type } = router
    const props: IRouteProps = {
      path,
      exact,
      component: loadable(loader, { fallback: <FallbackScreen /> }),
    }

    if (type === 'private') return <PrivateRoute key={key} {...props} />
    if (type === 'auth') return <AuthRoute key={key} {...props} />
    return <Route key={key} {...props} />
  }

  return (
    <Switch>
      {routes.filter((item) => item.type === 'auth').map((router, index) => routeSelector(router, index))}
      <Layout>
        {routes.filter((item) => item.type !== 'auth').map((router, index) => routeSelector(router, index))}
      </Layout>
    </Switch>
  )
}

export default Router
