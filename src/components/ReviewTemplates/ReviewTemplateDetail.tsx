import {
  Flex,
  Modal,
  ModalOverlay,
  ModalContent,
  ModalBody,
  InputGroup,
  InputLeftElement,
  Input,
  GridItem,
  Grid,
  Box,
  ModalHeader,
  Text,
  ModalFooter,
  Textarea,
  Divider,
  Image,
} from "@chakra-ui/react";
import { Icon } from "@chakra-ui/react";
import React from "react";
import { HiSearch, HiOutlineX, HiOutlineChevronLeft } from "react-icons/hi";
import Icon1 from "assets/img/icon.png"
import Icon2 from "assets/img/icon2.png"
import Icon3 from "assets/img/icon3.png"
import Icon4 from "assets/img/icon4.png"
import Icon5 from "assets/img/icon5.png"
interface IReviewTemplateModalProps {
  isOpen: boolean;
  onClose: () => void;
  onOpen: () => void;
}
const ReviewTemplatesList: React.FC<IReviewTemplateModalProps> = ({
  isOpen,
  onClose,
  onOpen,
}) => {
  return (
    <Modal
      blockScrollOnMount={false}
      isOpen={isOpen}
      onClose={onClose}
      size={"2xl"}
    >
      <ModalOverlay />
      <ModalContent  borderRadius={"12px"} py={"14px"}>
        <ModalHeader p={"0px"} />
        <ModalBody px={"14px"} py={'0px'}>
          <Flex
            alignItems={"center"}
            justifyContent={"space-between"}
            gap="6px"
          >
            <InputGroup>
              <InputLeftElement
                pointerEvents="none"
                children={
                  <Icon as={HiSearch} color={"#777777"} fontSize={"18px"} />
                }
              />
              <Input
                type="text"
                placeholder="Search review blocks and templates..."
                borderRadius={"6px"}
                backgroundColor={"#F5F5F5"}
              />
            </InputGroup>
            <Flex
              as="button"
              alignItems={"center"}
              gap={"6px"}
              justifyContent={"center"}
              onClick={onOpen}
              width={"30px"}
            >
              <Icon as={HiOutlineX} width={5} height={5} color={"black"} />
            </Flex>
          </Flex>
          <Grid templateColumns="repeat(3, 1fr)" gap={4} mt={"12px"}>
            <GridItem colSpan={1}>
              <Flex as='button' justifyContent={'start'} alignItems='center' gap='4px'>
                  <Icon as={HiOutlineChevronLeft} color={"#777777"} fontSize={"16px"} />
                  <Text as='span' fontSize={'14px'} fontWeight='500' lineHeight={'16px'} color='#777777'>Templates</Text>
              </Flex>
            </GridItem>
            <GridItem colSpan={2}>
              <Box
                maxHeight="550px"
                overflowY={"scroll"}
                paddingRight={"6px"}
                css={{
                  "&::-webkit-scrollbar": {
                    width: "4px",
                  },
                  "&::-webkit-scrollbar-track": {
                    width: "6px",
                  },
                  "&::-webkit-scrollbar-thumb": {
                    background: "#77747454",
                    borderRadius: "24px",
                  },
                }}
              >
                  <Text as='h3' fontWeight={500} fontSize='16px' mb={'8px'}>UX Update</Text>
                  <Text as="p" fontWeight={400} fontSize='14px' color='#777777' marginBottom={'25px'}>This template helps your team understand new user experience</Text>
                  <Text as="p" fontWeight={400} fontSize='14px' color='black' marginBottom={'8px'}>What’s working and what’s not?</Text>
                  <Box mb={'16px'}>
                    <Textarea placeholder='i.e This feels simpler than the previous experience.' />
                  </Box>
                  <Divider mb={'16px'}/>
                  <Box mb={'16px'}>
                    <Text mb="12px" as="p">How much of an improvement is this iteration from the previous version?</Text>
                    <Flex alignItems={'start'} gap={'10px'}>
                      <Image src={Icon1} alt='icon' width={'22px'} height={'22px'}/>
                      <Image src={Icon2} alt='icon2' width={'22px'} height={'22px'}/>
                      <Image src={Icon3} alt='icon3' width={'22px'} height={'22px'}/>
                      <Image src={Icon4} alt='icon4' width={'22px'} height={'22px'}/>
                      <Image src={Icon5} alt='icon5' width={'22px'} height={'22px'}/>
                    </Flex>
                  </Box>
                  <Divider mb="16px"/>
                  <Text mb="16px">Have we covered all the edge cases?</Text>
              </Box>
            </GridItem>
          </Grid>
        </ModalBody>
        <ModalFooter px={'14px'} pb="0px" pt="12px" borderTop="1px solid rgba(0, 0, 0, 0.15)">
          <Box
            as="button"
            borderRadius={"4px"}
            backgroundColor={"black"}
            px={"12px"}
            py={"6px"}
            color={"white"}
            fontSize={"14px"}
            lineHeight={"16px"}
            fontWeight={"400"}
          >
            Choose
          </Box>
        </ModalFooter>
      </ModalContent>
    </Modal>
  );
};

export default ReviewTemplatesList;
  