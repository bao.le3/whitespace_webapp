import { createSlice } from '@reduxjs/toolkit';
import { RootState } from '../../app/store';

export interface IWorkspacesState {
}

const initialState: IWorkspacesState = {
};

const workspacesSlice = createSlice({
  name: 'workspaces',
  initialState,
  reducers: {
  },
  extraReducers: {
    // Extra reducer comes here
  },
});
export const workspacesSelector = (
    state: RootState
) => state.workspaces;
export const workspacesActions =
workspacesSlice.actions;
export const workspacesReducer =
workspacesSlice.reducer;
export default workspacesSlice.reducer;
