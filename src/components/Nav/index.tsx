import React from "react";
import { HStack } from "@chakra-ui/react";
import NavLink from "components/NavLink";
const Nav: React.FC = () => {
  return (
    <HStack gap={'10px'}>
      <NavLink to={'/home'} name='Home'/>
      <NavLink to={'/teammates'} name='Teammates'/>
      <NavLink to={'/settings'} name='Settings'/>
    </HStack>
  );
};
export default Nav;
