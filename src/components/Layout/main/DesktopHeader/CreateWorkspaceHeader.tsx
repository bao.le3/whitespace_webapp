import { Box, Text, Flex, Img } from "@chakra-ui/react";
import { Select } from "chakra-react-select";
import Breadcrumb from "components/Breadcrumb";
import React from "react";
import { useAppSelector } from "app/hooks";
import { layoutSelector } from "../slice";
import SidebarToggleButton from "components/SidebarToggleButton";
import { useTranslation } from "react-i18next";
import SignUp from "assets/icons/SignUp.svg"
export const colourOptions = [
  { value: "V1", label: "V1"},
  { value: "V2", label: "V2"},
  { value: "V3", label: "V3"},
  { value: "V4", label: "V4"},
  { value: "V5", label: "V5"},
  { value: "V6", label: "V6"},
];
const CreateWorkspaceHeader: React.FC = () => {
  const { isDesktopSidebarOpen } = useAppSelector(layoutSelector);
  const { t } = useTranslation();
  return (
    <Flex
      justify="space-between"
      alignItems={"center"}
      as="header"
      px={"20px"}
      height={"50px"}
      pos={"fixed"}
      width={isDesktopSidebarOpen ? "calc(100% - 260px)" : "100%"}
      backgroundColor={"white"}
      zIndex={50}
      top="0"
    >
      <Flex
        justifyContent={"start"}
        alignItems={"center"}
        gap="12px"
        flexBasis={"40%"}
        height={'100%'}
      >
        <SidebarToggleButton />
        <Breadcrumb />
        <Select
          value={{label: 'V1', value: 'V1'}}
          name="version"
          useBasicStyles
          options={colourOptions}
          placeholder="V1"
          closeMenuOnSelect={false}
          size='sm'
          chakraStyles={{
            control: (provided, state) => ({
              ...provided,
              height: '20px',
              borderRadius: '8px'
            }),
          }}
        />
        <Flex as="button" gap="6px" justifyContent={'start'} alignItems={'center'} padding={'8px'} border={"1px solid #E1E1E1"} height={'30px'} borderRadius={'6px'}>
          <Img src={SignUp} alt={t('sync')}/>
          <Text as='span' fontSize={'14px'}>{t('sync')}</Text>
        </Flex>
      </Flex>
      <Flex alignItems={"center"} justifyContent={"start"} gap={"6px"}>
        <Box
          as="button"
          borderRadius={"4px"}
          backgroundColor={"#58B9F8"}
          px={"12px"}
          py={"6px"}
          color={"white"}
          fontSize={"14px"}
          lineHeight={"16px"}
          fontWeight={"400"}
        >
          {t("preview")}
        </Box>
        <Box
          as="button"
          borderRadius={"4px"}
          backgroundColor={"black"}
          px={"12px"}
          py={"6px"}
          color={"white"}
          fontSize={"14px"}
          lineHeight={"16px"}
          fontWeight={"400"}
        >
          {t("share")}
        </Box>
      </Flex>
    </Flex>
  );
};

export default CreateWorkspaceHeader;
