import React from "react";
import ContentEditable from "react-contenteditable";
import { Control, useController } from "react-hook-form";
import "./style.scss";
interface IInputProps {
  name: string;
  control: Control<any>;
}
interface IContentEditableFieldProps extends IInputProps {
  placeholder: string,
  className: string
}
const ContentEditableField: React.FC<IContentEditableFieldProps> = ({ name, control, placeholder, className }) => {
  const {
    field: { value, onChange, ref },
  } = useController({
    name,
    control,
  });
  return (
    <ContentEditable
      innerRef={ref}
      html={value} // innerHTML of the editable div
      disabled={false} // use true to disable editing
      onChange={onChange} // handle innerHTML change
      className={className}
      placeholder={placeholder}
    />
  );
};

export default ContentEditableField;
