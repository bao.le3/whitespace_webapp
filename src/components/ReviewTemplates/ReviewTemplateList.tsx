import {
    Flex,
    Modal,
    ModalOverlay,
    ModalContent,
    ModalBody,
    InputGroup,
    InputLeftElement,
    Input,
    GridItem,
    Grid,
    Box,
    ModalHeader,
    Text,
  } from "@chakra-ui/react";
  import { Icon } from "@chakra-ui/react";
  import React from "react";
  import { HiSearch, HiOutlineX } from "react-icons/hi";
  interface IReviewTemplateModalProps {
    isOpen: boolean,
    onClose: () => void,
    onOpen: () => void,
    onChooseTemplate: () => void,
  }
  const ReviewTemplatesList: React.FC<IReviewTemplateModalProps> = ({
    isOpen,
    onClose,
    onOpen,
    onChooseTemplate
  }) => {
    return (
      <Modal
        blockScrollOnMount={false}
        isOpen={isOpen}
        onClose={onClose}
        size={"2xl"}
      >
        <ModalOverlay />
        <ModalContent p={"14px"} borderRadius={"12px"}>
          <ModalHeader p={"0px"} />
          <ModalBody p={"0px"}>
            <Flex
              alignItems={"center"}
              justifyContent={"space-between"}
              gap="6px"
            >
              <InputGroup>
                <InputLeftElement
                  pointerEvents="none"
                  children={
                    <Icon as={HiSearch} color={"#777777"} fontSize={"18px"} />
                  }
                />
                <Input
                  type="text"
                  placeholder="Search review blocks and templates..."
                  borderRadius={"6px"}
                  backgroundColor={"#F5F5F5"}
                />
              </InputGroup>
              <Flex
                as="button"
                alignItems={"center"}
                gap={"6px"}
                justifyContent={"center"}
                onClick={onOpen}
                width={"30px"}
              >
                <Icon as={HiOutlineX} width={5} height={5} color={"black"} />
              </Flex>
            </Flex>
            <Grid templateColumns="repeat(3, 1fr)" gap={4} mt={"12px"}>
              <GridItem colSpan={1}>
                <Box
                  cursor={"pointer"}
                  backgroundColor={"#F5F5F5"}
                  borderRadius={"4px"}
                  px={"12px"}
                  py={"10px"}
                  mb={"6px"}
                  fontWeight={600}
                  fontSize={"14px"}
                  lineHeight={"16px"}
                >
                  All Templates{" "}
                </Box>
                <Box
                  cursor={"pointer"}
                  transition={"all 0.3s"}
                  _hover={{ backgroundColor: "#F5F5F5", fontWeight: 600 }}
                  borderRadius={"4px"}
                  px={"12px"}
                  py={"10px"}
                  mb={"6px"}
                  fontSize={"14px"}
                  lineHeight={"14px"}
                >
                  General
                </Box>
                <Box
                  cursor={"pointer"}
                  transition={"all 0.3s"}
                  _hover={{ backgroundColor: "#F5F5F5", fontWeight: 600 }}
                  borderRadius={"4px"}
                  px={"12px"}
                  py={"10px"}
                  mb={"6px"}
                  fontSize={"14px"}
                  lineHeight={"14px"}
                >
                  User Experience
                </Box>
                <Box
                  cursor={"pointer"}
                  transition={"all 0.3s"}
                  _hover={{ backgroundColor: "#F5F5F5", fontWeight: 600 }}
                  borderRadius={"4px"}
                  px={"12px"}
                  py={"10px"}
                  mb={"6px"}
                  fontSize={"14px"}
                  lineHeight={"14px"}
                >
                  Visual Design
                </Box>
                <Box
                  cursor={"pointer"}
                  transition={"all 0.3s"}
                  _hover={{ backgroundColor: "#F5F5F5", fontWeight: 600 }}
                  borderRadius={"4px"}
                  px={"12px"}
                  py={"10px"}
                  mb={"6px"}
                  fontSize={"14px"}
                  lineHeight={"14px"}
                >
                  Engineering
                </Box>
                <Box
                  cursor={"pointer"}
                  transition={"all 0.3s"}
                  _hover={{ backgroundColor: "#F5F5F5", fontWeight: 600 }}
                  borderRadius={"4px"}
                  px={"12px"}
                  py={"10px"}
                  mb={"6px"}
                  fontSize={"14px"}
                  lineHeight={"14px"}
                >
                  Product
                </Box>
                <Box
                  cursor={"pointer"}
                  transition={"all 0.3s"}
                  _hover={{ backgroundColor: "#F5F5F5", fontWeight: 600 }}
                  borderRadius={"4px"}
                  px={"12px"}
                  py={"10px"}
                  mb={"6px"}
                  fontSize={"14px"}
                  lineHeight={"14px"}
                >
                  Business
                </Box>
              </GridItem>
              <GridItem colSpan={2}>
                <Grid
                  templateColumns="repeat(3, 1fr)"
                  gap={"14px"}
                  maxHeight="550px"
                  overflowY={"scroll"}
                  paddingRight={"6px"}
                  css={{
                    "&::-webkit-scrollbar": {
                      width: "4px",
                    },
                    "&::-webkit-scrollbar-track": {
                      width: "6px",
                    },
                    "&::-webkit-scrollbar-thumb": {
                      background: "#77747454",
                      borderRadius: "24px",
                    },
                  }}
                >
                   <GridItem w="100%">
                    <Box
                      height={"80px"}
                      width={"100%"}
                      backgroundColor={"#F7F6FA"}
                      borderRadius={"6px"}
                      mb={"6px"}
                      cursor={'pointer'}
                      onClick={onChooseTemplate}
                    ></Box>
                    <Text
                      as="h4"
                      fontWeight={600}
                      fontSize={"14px"}
                      lineHeight="16px"
                      cursor={'pointer'}
                      onClick={onChooseTemplate}
                    >
                      New Component
                    </Text>
                    <Text
                      as="p"
                      color={"#777777"}
                      fontSize="12px"
                      lineHeight={"14px"}
                      fontWeight="400"
                      mt={'2px'}
                    >
                      Used at notion
                    </Text>
                  </GridItem>
                  <GridItem w="100%">
                    <Box
                      height={"80px"}
                      width={"100%"}
                      backgroundColor={"#F7F6FA"}
                      borderRadius={"6px"}
                      mb={"6px"}
                      cursor={'pointer'}
                      onClick={onChooseTemplate}
                    ></Box>
                    <Text
                      as="h4"
                      fontWeight={600}
                      fontSize={"14px"}
                      lineHeight="16px"
                      cursor={'pointer'}
                      onClick={onChooseTemplate}
                    >
                      New Component
                    </Text>
                    <Text
                      as="p"
                      color={"#777777"}
                      fontSize="12px"
                      lineHeight={"14px"}
                      fontWeight="400"
                      mt={'2px'}
                    >
                      Used at notion
                    </Text>
                  </GridItem>
                  <GridItem w="100%">
                    <Box
                      height={"80px"}
                      width={"100%"}
                      backgroundColor={"#F7F6FA"}
                      borderRadius={"6px"}
                      mb={"6px"}
                      cursor={'pointer'}
                      onClick={onChooseTemplate}
                    ></Box>
                    <Text
                      as="h4"
                      fontWeight={600}
                      fontSize={"14px"}
                      lineHeight="16px"
                      cursor={'pointer'}
                      onClick={onChooseTemplate}
                    >
                      New Component
                    </Text>
                    <Text
                      as="p"
                      color={"#777777"}
                      fontSize="12px"
                      lineHeight={"14px"}
                      fontWeight="400"
                      mt={'2px'}
                    >
                      Used at notion
                    </Text>
                  </GridItem>
                  <GridItem w="100%">
                    <Box
                      height={"80px"}
                      width={"100%"}
                      backgroundColor={"#F7F6FA"}
                      borderRadius={"6px"}
                      mb={"6px"}
                      cursor={'pointer'}
                      onClick={onChooseTemplate}
                    ></Box>
                    <Text
                      as="h4"
                      fontWeight={600}
                      fontSize={"14px"}
                      lineHeight="16px"
                      cursor={'pointer'}
                      onClick={onChooseTemplate}
                    >
                      New Component
                    </Text>
                    <Text
                      as="p"
                      color={"#777777"}
                      fontSize="12px"
                      lineHeight={"14px"}
                      fontWeight="400"
                      mt={'2px'}
                    >
                      Used at notion
                    </Text>
                  </GridItem>
                  <GridItem w="100%">
                    <Box
                      height={"80px"}
                      width={"100%"}
                      backgroundColor={"#F7F6FA"}
                      borderRadius={"6px"}
                      mb={"6px"}
                      cursor={'pointer'}
                      onClick={onChooseTemplate}
                    ></Box>
                    <Text
                      as="h4"
                      fontWeight={600}
                      fontSize={"14px"}
                      lineHeight="16px"
                      cursor={'pointer'}
                      onClick={onChooseTemplate}
                    >
                      New Component
                    </Text>
                    <Text
                      as="p"
                      color={"#777777"}
                      fontSize="12px"
                      lineHeight={"14px"}
                      fontWeight="400"
                      mt={'2px'}
                    >
                      Used at notion
                    </Text>
                  </GridItem>
                  <GridItem w="100%">
                    <Box
                      height={"80px"}
                      width={"100%"}
                      backgroundColor={"#F7F6FA"}
                      borderRadius={"6px"}
                      mb={"6px"}
                      cursor={'pointer'}
                      onClick={onChooseTemplate}
                    ></Box>
                    <Text
                      as="h4"
                      fontWeight={600}
                      fontSize={"14px"}
                      lineHeight="16px"
                      cursor={'pointer'}
                      onClick={onChooseTemplate}
                    >
                      New Component
                    </Text>
                    <Text
                      as="p"
                      color={"#777777"}
                      fontSize="12px"
                      lineHeight={"14px"}
                      fontWeight="400"
                      mt={'2px'}
                    >
                      Used at notion
                    </Text>
                  </GridItem>
                  <GridItem w="100%">
                    <Box
                      height={"80px"}
                      width={"100%"}
                      backgroundColor={"#F7F6FA"}
                      borderRadius={"6px"}
                      mb={"6px"}
                      cursor={'pointer'}
                      onClick={onChooseTemplate}
                    ></Box>
                    <Text
                      as="h4"
                      fontWeight={600}
                      fontSize={"14px"}
                      lineHeight="16px"
                      cursor={'pointer'}
                      onClick={onChooseTemplate}
                    >
                      New Component
                    </Text>
                    <Text
                      as="p"
                      color={"#777777"}
                      fontSize="12px"
                      lineHeight={"14px"}
                      fontWeight="400"
                      mt={'2px'}
                    >
                      Used at notion
                    </Text>
                  </GridItem>
                  <GridItem w="100%">
                    <Box
                      height={"80px"}
                      width={"100%"}
                      backgroundColor={"#F7F6FA"}
                      borderRadius={"6px"}
                      mb={"6px"}
                      cursor={'pointer'}
                      onClick={onChooseTemplate}
                    ></Box>
                    <Text
                      as="h4"
                      fontWeight={600}
                      fontSize={"14px"}
                      lineHeight="16px"
                      cursor={'pointer'}
                      onClick={onChooseTemplate}
                    >
                      New Component
                    </Text>
                    <Text
                      as="p"
                      color={"#777777"}
                      fontSize="12px"
                      lineHeight={"14px"}
                      fontWeight="400"
                      mt={'2px'}
                    >
                      Used at notion
                    </Text>
                  </GridItem>
                  <GridItem w="100%">
                    <Box
                      height={"80px"}
                      width={"100%"}
                      backgroundColor={"#F7F6FA"}
                      borderRadius={"6px"}
                      mb={"6px"}
                      cursor={'pointer'}
                      onClick={onChooseTemplate}
                    ></Box>
                    <Text
                      as="h4"
                      fontWeight={600}
                      fontSize={"14px"}
                      lineHeight="16px"
                      cursor={'pointer'}
                      onClick={onChooseTemplate}
                    >
                      New Component
                    </Text>
                    <Text
                      as="p"
                      color={"#777777"}
                      fontSize="12px"
                      lineHeight={"14px"}
                      fontWeight="400"
                      mt={'2px'}
                    >
                      Used at notion
                    </Text>
                  </GridItem>
                  <GridItem w="100%">
                    <Box
                      height={"80px"}
                      width={"100%"}
                      backgroundColor={"#F7F6FA"}
                      borderRadius={"6px"}
                      mb={"6px"}
                      cursor={'pointer'}
                      onClick={onChooseTemplate}
                    ></Box>
                    <Text
                      as="h4"
                      fontWeight={600}
                      fontSize={"14px"}
                      lineHeight="16px"
                      cursor={'pointer'}
                      onClick={onChooseTemplate}
                    >
                      New Component
                    </Text>
                    <Text
                      as="p"
                      color={"#777777"}
                      fontSize="12px"
                      lineHeight={"14px"}
                      fontWeight="400"
                      mt={'2px'}
                    >
                      Used at notion
                    </Text>
                  </GridItem>
                  <GridItem w="100%">
                    <Box
                      height={"80px"}
                      width={"100%"}
                      backgroundColor={"#F7F6FA"}
                      borderRadius={"6px"}
                      mb={"6px"}
                      cursor={'pointer'}
                      onClick={onChooseTemplate}
                    ></Box>
                    <Text
                      as="h4"
                      fontWeight={600}
                      fontSize={"14px"}
                      lineHeight="16px"
                      cursor={'pointer'}
                      onClick={onChooseTemplate}
                    >
                      New Component
                    </Text>
                    <Text
                      as="p"
                      color={"#777777"}
                      fontSize="12px"
                      lineHeight={"14px"}
                      fontWeight="400"
                      mt={'2px'}
                    >
                      Used at notion
                    </Text>
                  </GridItem>
                </Grid>
              </GridItem>
            </Grid>
          </ModalBody>
        </ModalContent>
      </Modal>
    );
  };
  
  export default ReviewTemplatesList;
  