import {
  Breadcrumb as CharkaBreadcrumb,
  BreadcrumbItem,
  BreadcrumbLink,
  Flex,
} from "@chakra-ui/react";
import React from "react";
import { Icon } from "@chakra-ui/react";
import { HiOutlineChevronLeft } from "react-icons/hi";
import { useTranslation } from "react-i18next";
import { Link } from "react-router-dom";
import { CREATE_WORKSPACE_ROUTE, WORKSPACES_ROUTE } from "constant/router";
const Breadcrumb: React.FC = () => {
  const {t} = useTranslation();
  return (
    <Flex as="button" justifyContent={"start"} alignItems="center" gap="8px">
      <Icon as={HiOutlineChevronLeft} color={"#777777"} fontSize={"16px"} />
      <CharkaBreadcrumb>
        <BreadcrumbItem>
          <BreadcrumbLink as='span' fontSize={"14px"} color={'#BBBBBB'} textTransform={'capitalize'}>
            <Link to={WORKSPACES_ROUTE}>{t('product')}</Link>
          </BreadcrumbLink>
        </BreadcrumbItem>

        <BreadcrumbItem>
          <BreadcrumbLink as='span' fontSize={"14px"} color={'#BBBBBB'}>
            <Link to={WORKSPACES_ROUTE}>{t('riders')}</Link>
          </BreadcrumbLink>
        </BreadcrumbItem>

        <BreadcrumbItem as='span' isCurrentPage>
          <BreadcrumbLink fontSize={"14px"} color='#000000'>
            <Link to={CREATE_WORKSPACE_ROUTE}>{t('untitled')}</Link>
          </BreadcrumbLink>
        </BreadcrumbItem>
      </CharkaBreadcrumb>
    </Flex>
  );
};

export default Breadcrumb;
