import {Flex } from "@chakra-ui/react";
import React, {useState} from "react";
import { Icon } from "@chakra-ui/react";
import { BiListUl, BiGridAlt } from "react-icons/bi";
interface ViewButtonProps{
    icon: any,
    style: {[key: string]: string | number},
    view: 'list' | 'grid',
    currentView: 'list' | 'grid', 
    handleOnClick: () => void
}
const ViewButton: React.FC<ViewButtonProps> = ({icon, style, view, currentView, handleOnClick}) => {
    return (<Flex
        onClick={handleOnClick}
        as="button"
        backgroundColor={currentView === view ? 'black' : 'white'}
        width={'36px'}
        height={'36px'}
        justifyContent={'center'}
        alignItems={'center'}
        borderRadius={'20px'}
        {...style}
      >
        <Icon as={icon} color={currentView === view ? 'white' : 'black'} width={'1.2em'} height={'1.2em'}/>
      </Flex>)
};
const ViewToggleButton: React.FC = () => {
  const [view, setView] = useState<'list' | 'grid'>('list');  
  return (
    <Flex justifyContent={"start"} alignItems={"center"}>
      <ViewButton view='list' currentView={view} icon={BiListUl} style={{borderBottomRightRadius: '0px', borderTopRightRadius: '0px'}} handleOnClick={() => {setView('list')}}/>
      <ViewButton view='grid' currentView={view} icon={BiGridAlt} style={{borderBottomLeftRadius: '0px', borderTopLeftRadius: '0px'}} handleOnClick={() => {setView('grid')}}/>  
    </Flex>
  );
};

export default ViewToggleButton;
