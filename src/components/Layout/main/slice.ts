import { createSlice, PayloadAction } from "@reduxjs/toolkit";
import { RootState } from "app/store";

export interface ILayoutState {
  isMobileSidebarOpen: boolean;
  isDesktopSidebarOpen: boolean;
  isCollapsedDektopSidebarOpen: boolean;
}

const initialState: ILayoutState = {
  isMobileSidebarOpen: false,
  isDesktopSidebarOpen: true,
  isCollapsedDektopSidebarOpen: false,
};

const layoutSlice = createSlice({
  name: "layout",
  initialState,
  reducers: {
    toggleDesktopSidebar(state, action: PayloadAction<{ isOpen: boolean }>) {
      state.isDesktopSidebarOpen = action?.payload?.isOpen;
      if (action?.payload?.isOpen) {
        state.isCollapsedDektopSidebarOpen = false;
      }
    },
    toggleMobileSidebar(state, action: PayloadAction<{ isOpen: boolean }>) {
      state.isDesktopSidebarOpen = action?.payload?.isOpen;
    },
    toggleCollapsedDesktopSidebar(
      state,
      action: PayloadAction<{ isOpen: boolean }>
    ) {
      if(state.isCollapsedDektopSidebarOpen !== action?.payload?.isOpen){
        state.isCollapsedDektopSidebarOpen = action?.payload?.isOpen;
      }
    },
  },
  extraReducers: {
    // Extra reducer comes here
  },
});
export const layoutSelector = (state: RootState) => state.layout;
export const layoutActions = layoutSlice.actions;
export const layoutReducer = layoutSlice.reducer;
export default layoutSlice.reducer;
